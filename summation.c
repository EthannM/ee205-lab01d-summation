///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int sum = 0;
   int n = atoi(argv[1]);
for(int i = n; i>0; i--)
{
   sum = (sum + i); 
}
printf("The sum of the digits from 1 to %d is %d\n", n, sum); 
   return 0;
}
